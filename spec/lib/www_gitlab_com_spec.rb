# frozen_string_literal: true

require 'spec_helper'
require 'www_gitlab_com'

RSpec.describe WwwGitLabCom do
  describe '.team_from_www' do
    it 'fetches team from www-gitlab-com data' do
      expect(WwwGitLabCom.team_from_www['dzaporozhets'].keys).to include('departments')
    end
  end
end
