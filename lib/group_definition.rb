# frozen_string_literal: true

module GroupDefinition
  module_function

  # Those are not in constants because then they can be lazily loaded and
  # garbage collected when needed. All the groups can be found at:
  # https://about.gitlab.com/handbook/product/categories/
  def group_access
    {
      assignees: %w[@jeremy @dennis @lmcandrew @amandakhughes],
      labels: ['group::access']
    }
  end

  def group_import
    {
      assignees: %w[@hdelalic @dennis @lmcandrew],
      labels: ['group::import']
    }
  end

  def group_analytics
    {
      mentions: %w[@dennis @djensen @npost @jshackelford @wortschi],
      assignees: %w[@dennis @djensen @jshackelford @wortschi],
      labels: ['group::analytics']
    }
  end

  def group_compliance
    {
      assignees: %w[@mattgonzales @dennis @djensen],
      labels: ['group::compliance']
    }
  end

  def group_spaces
    {
      assignees: %w[@tipyn @dennis @lmcandrew @amandakhughes],
      labels: ['group::spaces']
    }
  end

  def group_project_management
    {
      assignees: %w[@gweaver @donaldcook @johnhope @hollyreynolds],
      labels: ['group::project management']
    }
  end

  def group_portfolio_management
    {
      assignees: %w[@kokeefe @donaldcook @johnhope @uhlexsis],
      labels: ['group::portfolio management']
    }
  end

  def group_certify
    {
      assignees: %w[@mjwood @donaldcook @johnhope @nickbrandt],
      labels: ['group::certify']
    }
  end

  def group_source_code
    {
      mentions: %w[@danielgruesso @m_gill @andr3 @nick.thomas @pedroms],
      assignees: %w[@danielgruesso @andr3 @nick.thomas @pedroms],
      labels: ['group::source code']
    }
  end

  def group_knowledge
    {
      assignees: %w[@cdybenko @dsatcher @rkuba @mnearents],
      labels: ['group::knowledge']
    }
  end

  def group_editor
    {
      assignees: %w[@phikai @dsatcher @rkuba @mnichols1 @fjsanpedro],
      labels: ['group::editor']
    }
  end

  def group_static_site_editor
    {
      assignees: %w[@ericschurter @jeanduplessis @mle],
      labels: ['group::static site editor']
    }
  end

  def group_gitaly
    {
      assignees: %w[@jramsay @zj-gitlab @timzallmann],
      labels: ['group::gitaly']
    }
  end

  def group_gitter
    {
      assignees: %w[@ericschurter @MadLittleMods @viktomas @timzallmann @mnearents],
      labels: ['group::gitter']
    }
  end

  def group_ecosystem
    {
      assignees: %w[@deuley @leipert @lvanc],
      labels: ['group::ecosystem']
    }
  end

  def group_continuous_integration
    {
      assignees: %w[@thaoyeager @darbyfrey @dcipoletti @dimitrieh],
      labels: ['group::continuous integration']
    }
  end

  def group_runner
    {
      assignees: %w[@DarrenEastman @erushton @jj-ramirez],
      labels: ['group::runner']
    }
  end

  def group_testing
    {
      assignees: %w[@jheimbuck_gl @rickywiens @dcipoletti @jj-ramirez],
      labels: ['group::testing']
    }
  end

  def group_package
    {
      assignees: %w[@trizzi @jhampton @icamacho],
      labels: ['group::package']
    }
  end

  def group_progressive_delivery
    {
      assignees: %w[@ogolowinski @csouthard @nicolewilliams @dimitrieh],
      labels: ['group::progressive delivery']
    }
  end

  def group_release_management
    {
      assignees: %w[@jmeshell @sean_carroll @nicolewilliams @rayana],
      labels: ['group::release management']
    }
  end

  def group_configure
    {
      assignees: %w[@nagyv-gitlab @nicholasklick @mvrachni],
      labels: ['group::configure']
    }
  end

  def group_apm
    {
      mentions: %w[@dhershkovitch @mnohr @ClemMakesApps @kbychu @nadia_sotnikova],
      assignees: %w[@dhershkovitch @mnohr @ClemMakesApps @nadia_sotnikova],
      labels: ['group::apm']
    }
  end

  def group_health
    {
      mentions: %w[@sarahwaldner @crystalpoole @ClemMakesApps @kbychu @ameliabauerly],
      assignees: %w[@sarahwaldner @crystalpoole @ClemMakesApps @ameliabauerly],
      labels: ['group::health']
    }
  end

  def group_static_analysis
    {
      assignees: %w[@tmccaslin @twoodham @nmccorrison @cam.x],
      labels: ['group::static analysis']
    }
  end

  def group_dynamic_analysis
    {
      assignees: %w[@sethgitlab @nmccorrison @derekferguson @annabeldunstone],
      labels: ['group::dynamic analysis']
    }
  end

  def group_composition_analysis
    {
      assignees: %w[@NicoleSchwartz @gonzoyumo @nmccorrison @kmann],
      labels: ['group::composition analysis']
    }
  end

  def group_attack_surface
    {
      assignees: %w[@stkerr @sethgitlab @kmann],
      labels: ['group::attack surface']
    }
  end

  def group_runtime_application_security
    {
      assignees: %w[@sam.white @twoodham @lkerr @beckalippert],
      labels: ['group::runtime application security']
    }
  end

  def group_threat_management
    {
      assignees: %w[@matt_wilson @twoodham @lkerr @beckalippert],
      labels: ['group::threat management']
    }
  end

  def group_application_infrastructure_security
    {
      assignees: %w[@sam.white @twoodham @lkerr @andyvolpe],
      labels: ['group::application infrastructure security']
    }
  end

  def group_anomaly_detection
    {
      assignees: %w[@matt_wilson @twoodham @nmccorrison @andyvolpe],
      labels: ['group::anomaly detection']
    }
  end

  def group_acquisition
    {
      mentions: %w[@jstava @jeromezng @bmarnane @esybrant],
      assignees: %w[@jstava @jeromezng @esybrant],
      labels: ['group::acquisition']
    }
  end

  def group_expansion
    {
      assignees: %w[@timhey @pcalder @matejlatin],
      labels: ['group::expansion']
    }
  end

  def group_conversion
    {
      mentions: %w[@s_awezec @jeromezng @bmarnane @kcomoli],
      assignees: %w[@s_awezec @jeromezng @kcomoli],
      labels: ['group::conversion']
    }
  end

  def group_retention
    {
      assignees: %w[@mkarampalas @pcalder @timnoah],
      labels: ['group::retention']
    }
  end

  def group_fulfillment
    {
      assignees: %w[@mkarampalas @jameslopez @chris_baus @jackib @amandarueda],
      labels: ['group::fulfillment']
    }
  end

  def group_telemetry
    {
      assignees: %w[@timhey @jeromezng @sid_reddy @jackib],
      labels: ['group::telemetry']
    }
  end

  def group_distribution
    {
      assignees: %w[@ljlane @mendeni @sunjungp],
      labels: ['group::distribution']
    }
  end

  def group_geo
    {
      assignees: %w[@fzimmer @nhxnguyen],
      labels: ['group::geo']
    }
  end

  def group_memory
    {
      assignees: %w[@joshlambert @craig-gomes @jackib],
      labels: ['group::memory']
    }
  end

  def group_search
    {
      assignees: %w[@JohnMcGuire @changzhengliu],
      labels: ['group::search']
    }
  end

  def group_database
    {
      assignees: %w[@joshlambert @craig-gomes @jackib],
      labels: ['group::database']
    }
  end

  def group_quality
    {
      assignees: %w[@at.ramya],
      labels: ['Quality', 'QA']
    }
  end
end
